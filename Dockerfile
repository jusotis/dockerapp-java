FROM openjdk:8
COPY ./out/production/dockerapp-java/ /tmp
WORKDIR /tmp
ENTRYPOINT ["java","com.example.helloworld.HelloDocker"]
